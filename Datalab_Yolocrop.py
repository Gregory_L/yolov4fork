

'''
    File name: Datalab_Yolo.py
    Author: Pierrick LORANG
    Date created: 29/01/2021
    Python version: 3.8.5
'''

# import time
# from PIL import Image, ImageDraw
# from models.tiny_yolo import TinyYoloNet
from Bound_it.yolov4fork.tool.utils_yolocrop import *
from Bound_it.yolov4fork.tool.torch_utils import *
from Bound_it.yolov4fork.tool.darknet2pytorch import Darknet
import Constants
import os

"""hyper parameters"""
use_cuda = True
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
YoloPath = Constants.YOLOPATH
YoloPath=os.fspath(YoloPath)

#algorithm : type of CNN algo (yolov4 yolov4-tiny yolov3 yolov-tiny). Change the cfgfile and the weightfile
#cfgfile : <file>.cfg (yolov4.cfg) | weightfile : <file>.weight (yolov4.weight) | img: <path_to_imagefile>.jpeg/png/jpg

###############################################################################
#RUN YOLO GET BOXES
###############################################################################

def Yolocrop_detect(img, label, algorithm='yolov4-tiny', use_cuda=False):

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    import cv2
    m = Darknet(YoloPath+'/cfg/'+algorithm+'.cfg')

    #m.print_network()
    m.load_weights(YoloPath+'/checkpoints/'+algorithm+'.weights')
    #print('Loading weights from %s... Done!' % (weightfile))
    
    imgfile=img
    imgname=os.path.split(imgfile)[1]
    
    if use_cuda:
        m.cuda()

    num_classes = m.num_classes
    if num_classes == 20:
        namesfile = YoloPath+'/data/voc.names'
    elif num_classes == 80:
        namesfile = YoloPath+'/data/coco.names'
    else:
        namesfile = YoloPath+'/data/x.names'
    class_names = load_class_names(namesfile)

    index_label = class_names.index(label)

    img = cv2.imread(imgfile)
    sized = cv2.resize(img, (m.width, m.height))

    for i in range(2):
        boxes = do_detect(m, sized, 0.4, 0.6, use_cuda)
        
    savename=None
    bb_datalab=get_boxes(imgfile,img, boxes[0], index_label, savename=savename, class_names=class_names)
    
    return bb_datalab


###############################################################################

###############################################################################
